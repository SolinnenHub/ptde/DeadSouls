# DeadSolus PTDE

DeadSolus PTDE is a fork of original DeadSouls project made for Minecraft: PTDE.

https://github.com/Darkyenus/DeadSouls

### Souls

By killing mobs, players earn experience called Souls. The more souls you have (the higher the game level), the stronger your character (this increase is not very significant) and the higher your position in the global ranking.

When a player dies, in his place, in addition to the dropped items, a green cloud appeared - souls that anyone can absorb, having completely absorbed the experience.

### Rank system and rating table

In addition to bonuses to characteristics, we have a reward for a truly strong soul: the title of Champion and access to private quarters with chests, charms and other goodies.

## How to build:

```bash
./build.sh
```
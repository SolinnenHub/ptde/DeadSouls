package com.darkyen.minecraft.db;

import com.darkyen.minecraft.DeadSouls;

import java.util.logging.Level;

public class Error {
    public static void close(DeadSouls plugin, Exception ex){
        plugin.getLogger().log(Level.SEVERE, "Failed to close MySQL connection: ", ex);
    }
}
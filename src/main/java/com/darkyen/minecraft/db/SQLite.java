package com.darkyen.minecraft.db;

import com.darkyen.minecraft.DeadSouls;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;


public class SQLite extends Database {

    final String dbname;

    public SQLite(DeadSouls instance) {
        super(instance);

        dbname = "database.db";
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public Connection getSQLConnection() {
        File dataFolder = new File(plugin.getDataFolder(), dbname);
        if (!dataFolder.exists()){
            try {
                dataFolder.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().log(Level.SEVERE, "File write error: "+dbname);
            }
        }
        try {
            if(connection!=null && !connection.isClosed()) {
                return connection;
            }
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + dataFolder);
            return connection;
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE,"SQLite exception on initialize", ex);
        } catch (ClassNotFoundException ex) {
            plugin.getLogger().log(Level.SEVERE, "You need the SQLite JBDC library. Google it. Put it in /lib folder.");
        }
        return null;
    }

    public void load() {
        connection = getSQLConnection();
        try {
            Statement s = connection.createStatement();
            String query = "CREATE TABLE IF NOT EXISTS " + Database.souls_table_name + " (" +
                    "`ownerUuid` VARCHAR(36) NOT NULL UNIQUE," +
                    "`worldUuid` VARCHAR(36) NOT NULL," +
                    "`x` REAL NOT NULL," +
                    "`y` REAL NOT NULL," +
                    "`z` REAL NOT NULL," +
                    "`timestamp` INTEGER NOT NULL," +
                    "`xp` INT NOT NULL," +
                    " PRIMARY KEY (`ownerUuid`)" +
                    ");";
            s.executeUpdate(query);

            query = "CREATE TABLE IF NOT EXISTS " + Database.leaderboard_table_name + " (" +
                    "`username` VARCHAR(40) NOT NULL UNIQUE," +
                    "`xp` INT NOT NULL," +
                    "`level` INT NOT NULL," +
                    "`timestamp` INTEGER NOT NULL," +
                    " PRIMARY KEY (`username`)" +
                    ");";
            s.executeUpdate(query);
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        initialize();
    }
}

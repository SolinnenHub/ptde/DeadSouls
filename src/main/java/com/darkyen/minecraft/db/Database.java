package com.darkyen.minecraft.db;

import com.darkyen.minecraft.DeadSouls;
import com.darkyen.minecraft.Soul;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;

public abstract class Database {
    final DeadSouls plugin;
    public Connection connection;

    public static final String souls_table_name = "souls";
    public static final String leaderboard_table_name = "leaderboard";

    public Database(DeadSouls instance) {
        plugin = instance;
    }

    public abstract Connection getSQLConnection();

    public abstract void load();

    public void initialize() {
        connection = getSQLConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + Database.souls_table_name + " WHERE ownerUuid = ?");
            ps.executeQuery();
            ps = connection.prepareStatement("SELECT * FROM " + Database.leaderboard_table_name + " WHERE username = ?");
            ResultSet rs = ps.executeQuery();
            close(ps, rs);
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, "Unable to retrieve connection", ex);
        }
    }

    private ArrayList<Soul> getSoulsFromDatabase() throws SQLException {
        ArrayList<Soul> dbSouls = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + Database.souls_table_name);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            dbSouls.add(new Soul(
                    UUID.fromString(rs.getString("ownerUuid")),
                    UUID.fromString(rs.getString("worldUuid")),
                    rs.getDouble("x"),
                    rs.getDouble("y"),
                    rs.getDouble("z"),
                    rs.getLong("timestamp"),
                    rs.getInt("xp")
            ));
        }
        try {
            ps.close();
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose, ex);
        }
        return dbSouls;
    }

    public HashMap<String, LeaderboardEntry> getLeaderboardEntries(){
        HashMap<String, LeaderboardEntry> entries = new HashMap<>();
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement("SELECT * FROM " + Database.leaderboard_table_name);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                entries.put(
                        rs.getString("username"),
                        new LeaderboardEntry(
                            rs.getString("username"),
                            rs.getLong("timestamp"),
                            rs.getInt("xp"),
                            rs.getInt("level")
                        )
                );
            }
            try {
                ps.close();
            } catch (SQLException e) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose, e);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entries;
    }

    public void saveSoulsToDatabase(HashMap<UUID, Soul> souls) {
        PreparedStatement ps = null;
        try {
            ArrayList<Soul> dbSouls = this.getSoulsFromDatabase();
            ArrayList<Soul> candidatesForDeletion = new ArrayList<>(dbSouls);

            for (Soul soul : souls.values()) {
                ps = this.putSoul(soul);
                candidatesForDeletion.removeIf(candidate -> soul.ownerUuid.equals(candidate.ownerUuid));
            }
            for (Soul candidate : candidatesForDeletion) {
                ps = this.removeSoul(candidate);
            }
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute, ex);
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose, ex);
            }
        }
    }

    public HashMap<UUID, Soul> getSouls() {
        try {
            HashMap<UUID, Soul> souls = new HashMap<>(100);
            for (Soul soul : getSoulsFromDatabase()) {
                souls.put(soul.ownerUuid, soul);
            }
            return souls;
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute, ex);
        }
        return new HashMap<>();
    }

    public void saveLeaderboardToDatabase(HashMap<String, LeaderboardEntry> entries) {
        PreparedStatement ps = null;
        try {
            for (LeaderboardEntry entry : entries.values()) {
                ps = this.saveLeaderboardEntryToDatabase(entry);
            }
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute, ex);
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose, ex);
            }
        }
    }

    private PreparedStatement removeSoul(Soul soul) throws SQLException {
        final String query = "DELETE FROM "+Database.souls_table_name+" WHERE ownerUuid='"+ soul.ownerUuid +"'";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.executeUpdate();
        return ps;
    }

    private PreparedStatement putSoul(Soul soul) throws SQLException {
        String query = "INSERT INTO "+Database.souls_table_name+"(ownerUuid, worldUuid, x, y, z, timestamp, xp)" +
                " VALUES(" +
                "'"+soul.ownerUuid+"'," +
                "'"+soul.worldUuid+"'," +
                ""+soul.x+"," +
                ""+soul.y+"," +
                ""+soul.z+"," +
                ""+soul.timestamp+"," +
                ""+soul.xp+")" +
                " ON CONFLICT(ownerUuid) DO UPDATE SET " +
                "worldUuid='"+soul.worldUuid+"'," +
                "x="+soul.x+"," +
                "y="+soul.y+"," +
                "z="+soul.z+"," +
                "timestamp="+soul.timestamp+"," +
                "xp="+soul.xp +
                " WHERE ownerUuid='"+soul.ownerUuid+"';)";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.executeUpdate();
        return ps;
    }

    private PreparedStatement saveLeaderboardEntryToDatabase(LeaderboardEntry entry) throws SQLException {
        String query;
        if (entry.xp > 0) {
            query = "INSERT INTO " + Database.leaderboard_table_name + "(username, xp, level, timestamp)" +
                    " VALUES(" +
                    "'" + entry.username + "'," +
                    entry.xp + "," +
                    entry.level + "," +
                    entry.timestamp + ")" +
                    " ON CONFLICT(username) DO UPDATE SET " +
                    "xp=" + entry.xp + "," +
                    "level=" + entry.level + "," +
                    "timestamp=" + entry.timestamp +
                    " WHERE username='" + entry.username + "';)";
        } else {
            query = "DELETE FROM " + Database.leaderboard_table_name + " WHERE username='" + entry.username + "';)";
        }
        PreparedStatement ps = connection.prepareStatement(query);
        ps.executeUpdate();
        return ps;
    }

    public void close(PreparedStatement ps, ResultSet rs){
        try {
            if (ps != null)
                ps.close();
            if (rs != null)
                rs.close();
        } catch (SQLException ex) {
            Error.close(plugin, ex);
        }
    }
}

package com.darkyen.minecraft.db;

public abstract class Errors {

    public static final String sqlConnectionExecute = "Couldn't execute MySQL statement: ";

    public static final String sqlConnectionClose = "Failed to close MySQL connection: ";

}

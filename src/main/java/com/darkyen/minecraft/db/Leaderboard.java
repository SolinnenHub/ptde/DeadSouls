package com.darkyen.minecraft.db;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.HashMap;

import static com.darkyen.minecraft.DeadSouls.MIN_LEVEL_TO_BE_CHAMPION;
import static com.darkyen.minecraft.DeadSouls.MIN_LEVEL_TO_HAVE_NORMAL_HP;

public class Leaderboard {

    public boolean isTopUpdated = false;

    public enum Status {
        CHAMPION,
        ELITE,
        NORMAL,
        PLEBEIAN
    }

    public static final int LEADERBOARD_SIZE = 5;

    private final HashMap<String, LeaderboardEntry> entries;
    private ArrayList<LeaderboardEntry> top;

    public Leaderboard(HashMap<String, LeaderboardEntry> entries) {
        this.entries = entries;
        this.top = rebuildTop(new ArrayList<>(this.entries.values()));
    }

    private ArrayList<LeaderboardEntry> rebuildTop(ArrayList<LeaderboardEntry> entries) {
        ArrayList<LeaderboardEntry> top = new ArrayList<>(LEADERBOARD_SIZE);
        LeaderboardEntry temp;
        if (!entries.isEmpty()) {
            for (int i = 0; i < LEADERBOARD_SIZE && i < entries.size(); i++) {
                top.add(entries.get(i));
                for (int j = i + 1; j < entries.size(); j++) {
                    if (entries.get(j).xp > top.get(i).xp) {
                        temp = top.get(i);
                        top.set(i, entries.get(j));
                        entries.set(j, temp);
                    }
                }
            }
        }
        this.isTopUpdated = true;
        return top;
    }

    private int getPositionInTheTop(String username) {
        if (top.isEmpty()) {
            return -1;
        }
        for (int i = 0; i < top.size(); i++) {
            if (top.get(i).username.equals(username)) {
                return i;
            }
        }
        return -1;
    }

    public void update(LeaderboardEntry entry) {
        this.entries.put(entry.username, entry);

        int positionInTheTop = getPositionInTheTop(entry.username);
        if (positionInTheTop >= 0) {
            if (((top.size() >= LEADERBOARD_SIZE)) && (top.get(LEADERBOARD_SIZE-1).xp > entry.xp)) {
                this.top = rebuildTop(new ArrayList<>(this.entries.values()));
            } else {
                this.top.set(positionInTheTop, entry);
                this.top = rebuildTop(this.top);
            }
        } else {
            if ((top.size() < LEADERBOARD_SIZE) || (top.get(LEADERBOARD_SIZE - 1).xp < entry.xp)) {
                this.top.add(entry);
                this.top = rebuildTop(this.top);
            }
        }
    }

    public ArrayList<LeaderboardEntry> getTop() {
        return this.top;
    }

    public HashMap<String, LeaderboardEntry> getEntries() {
        return this.entries;
    }

    public Status getStatus(String username) {
        LeaderboardEntry entry = entries.get(username);
        if (entry == null) {
            return Status.PLEBEIAN;
        }
        if (entry.level >= MIN_LEVEL_TO_BE_CHAMPION) {
            if (username.equals(top.get(0).username)) {
                return Status.CHAMPION;
            } else {
                return Status.ELITE;
            }
        } else {
            if (entry.level >= MIN_LEVEL_TO_HAVE_NORMAL_HP) {
                return Status.NORMAL;
            } else {
                return Status.PLEBEIAN;
            }
        }
    }

    public static String getColorPrefix(Status status) {
        if (status == Status.CHAMPION) {
            return ""+ ChatColor.GOLD;
        } else if (status == Status.ELITE) {
            return ""+ChatColor.BLUE;
        } else if (status == Status.NORMAL) {
            return ""+ChatColor.WHITE;
        } else {
            return ""+ChatColor.GRAY;
        }
    }
}

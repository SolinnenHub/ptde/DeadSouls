package com.darkyen.minecraft.db;

import org.jetbrains.annotations.NotNull;

public class LeaderboardEntry implements Comparable<LeaderboardEntry> {

    public final String username;
    public final int xp, level;
    public final long timestamp;

    public LeaderboardEntry(String username, long timestamp, int xp, int level) {
        this.username = username;
        this.xp = xp;
        this.level = level;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "LeaderboardEntry{" +
                ", xp=" + xp +
                ", level=" + level +
                ", timestamp=" + timestamp +
                '}';
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(@NotNull LeaderboardEntry o) {
        if (this.xp < o.xp) {
            return -1;
        } else if (this.xp > o.xp) {
            return 1;
        }
        return 0;
    }
}



package com.darkyen.minecraft;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.NumberConversions;
import org.jetbrains.annotations.NotNull;

public class Util {

    public static double squaredDistanceFlat(Location source, Location destination) {
        return NumberConversions.square(source.getX()-destination.getX()) +
                NumberConversions.square(source.getZ()-destination.getZ());
    }

    public static double squaredDistanceSpatial(Location source, Location destination, double yScale) {
        return NumberConversions.square(source.getX()-destination.getX()) +
                NumberConversions.square((source.getY()-destination.getY()) * yScale) +
                NumberConversions.square(source.getZ()-destination.getZ());
    }

    public static float fit(float value, float min, float max) {
        if (value <= min) {
            return min;
        } else return Math.min(value, max);
    }

    static int getExpToLevel(int expLevel) {
        // From Spigot source
        if (expLevel >= 30) return 112 + (expLevel - 30) * 9;
        else if (expLevel >= 15) return 37 + (expLevel - 15) * 5;
        else return 7 + expLevel * 2;
    }

    static int getExperienceToReach(int level) {
        // From https://minecraft.gamepedia.com/Experience (16. 7. 2019, Minecraft 1.14.2)
        final int level2 = level * level;
        if (level <= 16) {
            return level2 + 6*level;
        } else if (level <= 31) {
            return level2 * 2 + level2/2 - 40*level - level/2 + 360;
        } else {
            return level2 * 4 + level2/2 - 162 * level - level/2 + 2220;
        }
    }

    public static int getTotalExperience(@NotNull Player player) {
        // Can't use player.getTotalExperience(), because that does not properly handle XP added via "/xp add level",
        // and, most importantly, it does not factor in experience spent on enchanting.
        return getExperienceToReach(player.getLevel()) + Math.round(getExpToLevel(player.getLevel()) * player.getExp());
    }
}

package com.darkyen.minecraft;

import com.darkyen.minecraft.db.*;
import com.darkyen.minecraft.listeners.ExpChangeListener;
import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Lectern;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.NumberConversions;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;

import static com.darkyen.minecraft.Util.*;

public class DeadSouls extends JavaPlugin implements Listener {

    public static DeadSouls inst;
    private Database db;

    private final HashMap<UUID, Location> playersLastSafeLocations = new HashMap<>(50);
    private HashMap<UUID, Soul> souls = new HashMap<>(100);

    public Leaderboard leaderboard;

    private Hologram hologram;
    public static final String soulsChar = "Ⓢ";
    private final ArrayList<Lectern> lecterns = new ArrayList<>();
    private ArrayList<String> loreStrings;
    private long wipesCount = 0;

    public static final int MIN_LEVEL_TO_BE_CHAMPION = 50;
    public static final int MIN_LEVEL_TO_HAVE_NORMAL_HP = 10;

    private static final double COLLECTION_DISTANCE = NumberConversions.square(1);
    private static final double VISION_DISTANCE_SQUARED = NumberConversions.square(90);

    private final Random random = new Random();

    public World world_bonfire;
    public World world_world;

    @Override
    public void onEnable() {
        inst = this;

        final FileConfiguration config = getConfig();

        saveDefaultConfig();

        this.db = new SQLite(this);
        this.db.load();

        this.leaderboard = new Leaderboard(this.db.getLeaderboardEntries());
        this.souls = db.getSouls();

        world_bonfire = Objects.requireNonNull(DeadSouls.inst.getServer().getWorld("Bonfire"));
        world_world = Objects.requireNonNull(DeadSouls.inst.getServer().getWorld("world"));

        String worldName = config.getString("leaderboard.hd.world");
        if (worldName == null || worldName.isEmpty()) {
            getLogger().log(Level.SEVERE, "Leaderboard world was not specified in config. The plugin will be disabled.");
            this.setEnabled(false);
            return;
        }
        World world = getServer().getWorld(worldName);
        if (world == null) {
            getLogger().log(Level.SEVERE, "Leaderboard world ["+worldName+"] does not exist. The plugin will be disabled.");
            this.setEnabled(false);
            return;
        }
        this.hologram = HologramsAPI.createHologram(this, new Location(
                getServer().getWorld(worldName),
                config.getDouble("leaderboard.hd.x"),
                config.getDouble("leaderboard.hd.y"),
                config.getDouble("leaderboard.hd.z")
        ));

        ConfigurationSection section = config.getConfigurationSection("lecterns");
        assert section != null;
        Set<String> collection = section.getKeys(false);
        for (String i : collection) {
            worldName = config.getString("lecterns."+i+".world");
            assert worldName != null;
            world = getServer().getWorld(worldName);
            assert world != null;
            BlockState block = world.getBlockAt(
                    config.getInt("lecterns."+i+".x"),
                    config.getInt("lecterns."+i+".y"),
                    config.getInt("lecterns."+i+".z")
            ).getState();
            if (block instanceof Lectern) {
                Lectern lectern = (Lectern) block;
                this.lecterns.add(lectern);
            }
        }

        loreStrings = (ArrayList<String>) config.getStringList("texts");

        String path = "plugins/DeadSouls/WipesCount.txt";
        try {
            wipesCount = WorldAge.loadData(path);
        } catch (IOException e) {
            WorldAge.beginNewEra(path);
        }

        final Server server = getServer();
        server.getScheduler().runTaskTimer(this, this::processPlayers, 0, 15);
        server.getScheduler().runTaskTimer(this, this::autoUpdateLecternsAndHologram, 0, 21*2);
        server.getScheduler().runTaskTimer(this, this::autoUpdateLecterns, 0, 20*60*60);

        PluginManager plugman = getServer().getPluginManager();
        plugman.registerEvents(this, this);
        plugman.registerEvents(new ExpChangeListener(), this);
    }

    public ItemStack getLecternBook() {
        ItemStack item = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta meta = (BookMeta) item.getItemMeta();
        assert meta != null;
        meta.setAuthor("");
        meta.setTitle("Хроники");

        StringBuilder firstPage = new StringBuilder("Сильнейшие души:\n");
        for (int i = 0; i < leaderboard.getTop().size(); i++) {
            firstPage.append(i+1).append(". ").append(ChatColor.BLACK).append(leaderboard.getTop().get(i).username).append(" ").append(soulsChar).append(leaderboard.getTop().get(i).level).append("\n");
        }
        firstPage.append(ChatColor.BLACK).append("\nПерерождений мира: ").append(ChatColor.BLUE).append(wipesCount).append(ChatColor.BLACK).append(".\n");

        int lecternLoreChangeInterval = 1000*60*60*24*3;
        meta.setPages(Arrays.asList(
                firstPage.toString(),
                loreStrings.get((int) ((System.currentTimeMillis()/lecternLoreChangeInterval) % loreStrings.size()))
        ));
        item.setItemMeta(meta);
        return item;
    }

    private void autoUpdateLecternsAndHologram() {
        if (leaderboard.isTopUpdated) {
            leaderboard.isTopUpdated = false;
            for (Lectern lectern : lecterns) {
                lectern.getInventory().setItem(0, this.getLecternBook());
            }

            hologram.clearLines();
            hologram.appendTextLine("Сильнейшие души");
            for (LeaderboardEntry entry : leaderboard.getTop()) {
                Leaderboard.Status status = leaderboard.getStatus(entry.username);
                hologram.appendTextLine(Leaderboard.getColorPrefix(status) + entry.username + ChatColor.RESET + " " + soulsChar + entry.level);
            }
        }
    }

    private void autoUpdateLecterns() {
        for (Lectern lectern : lecterns) {
            lectern.getInventory().setItem(0, this.getLecternBook());
        }
    }

    private void processPlayers() {
        for (Player player : this.getServer().getOnlinePlayers()) {
            final UUID uuid = player.getUniqueId();
            final World world = player.getWorld();

            // Last safe location
            if (!player.isDead() && player.getGameMode() != GameMode.SPECTATOR) {
                final Location loc = player.getLocation();
                final Block underPlayer = world.getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ());
                if (underPlayer.getType().isSolid()) {
                    final Block atPlayer = world.getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
                    if (atPlayer.getType() != Material.LAVA) {
                        playersLastSafeLocations.put(uuid, loc);
                    }
                }
            }

            // Update visible souls
            final ArrayList<Soul> visibleSouls = new ArrayList<>();
            for (Soul soul : this.souls.values()) {
                if (world.getUID().equals(soul.worldUuid)) {
                    if (squaredDistanceFlat(player.getLocation(), new Location(world, soul.x, soul.y, soul.z)) < VISION_DISTANCE_SQUARED) {
                        visibleSouls.add(soul);
                    }
                }
            }
            if (visibleSouls.isEmpty()) {
                continue;
            }

            // Send particles
            final int soulCount = visibleSouls.size();
            int remainingSoulsToShow = 16;
            for (int i = 0; i < soulCount && remainingSoulsToShow > 0; i++) {
                final Soul soul = visibleSouls.get(i);

                float soulStrength = Util.fit(soul.xp / 60f, 0, 1);

                player.spawnParticle(Particle.REDSTONE, new Location(world, soul.x, soul.y, soul.z), 10 + (int) (soulStrength * 18), 0.12, 0.14, 0.12,
                        new Particle.DustOptions(Color.fromRGB(206, 248, 198), 1.5f + 1.5f * soulStrength));
                remainingSoulsToShow--;
            }

            // Process collisions
            if (!player.isDead()) {
                for (Soul soul : visibleSouls) {
                    final Location soulLocation = new Location(world, soul.x, soul.y, soul.z);
                    if (squaredDistanceSpatial(soulLocation, player.getLocation(), 0.4) < COLLECTION_DISTANCE) {
                        souls.remove(soul.ownerUuid);
                        if (soul.xp > 0) {
                            player.giveExp(soul.xp);
                        }
                        player.sendTitle(ChatColor.AQUA + "ДУШИ ПОЛУЧЕНЫ", "", 7, 30, 40);
                        player.playSound(soulLocation, "block.end_portal.spawn", 1f, 1f);
                        break;
                    } else if (this.random.nextInt(20) == 0) {
                        player.playSound(soulLocation, "block.beacon.ambient", 2.7f, 0.75f);
                    }
                }
            }
        }
    }

    @Override
    public void onDisable() {
        this.db.saveSoulsToDatabase(this.souls);
        this.db.saveLeaderboardToDatabase(this.leaderboard.getEntries());
        try {
            if (!this.db.connection.isClosed()) {
                this.db.connection.close();
            }
        } catch (SQLException ex) {
            getLogger().log(Level.SEVERE, Errors.sqlConnectionClose, ex);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    private void onPlayerChangedWorldEvent(PlayerChangedWorldEvent event) {
        this.playersLastSafeLocations.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.LOW)
    private void onPlayerQuitEvent(PlayerQuitEvent event) {
        this.playersLastSafeLocations.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onPlayerDeath(PlayerDeathEvent event) {
        final Player player = event.getEntity();
        final World world = player.getWorld();

        if (!event.getKeepLevel() && !Boolean.TRUE.equals(world.getGameRuleValue(GameRule.KEEP_INVENTORY))) {
            int soulXp = getTotalExperience(player);

            final UUID ownerUuid = player.getUniqueId();
            Location soulLocation = playersLastSafeLocations.getOrDefault(ownerUuid, player.getLocation());

            final double SOUL_Y_OFFSET = 0.8;
            souls.put(ownerUuid, new Soul(ownerUuid, world.getUID(),
                    soulLocation.getX(), soulLocation.getY()+SOUL_Y_OFFSET, soulLocation.getZ(), System.currentTimeMillis(), soulXp));
            this.playersLastSafeLocations.remove(ownerUuid);
            world.playSound(soulLocation, "block.bell.resonate", SoundCategory.MASTER, 1.1f, 1.7f);

            event.setNewExp(0);
            event.setNewLevel(0);
            event.setNewTotalExp(0);
            event.setDroppedExp(0);
        }
    }
}

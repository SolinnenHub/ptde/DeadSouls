package com.darkyen.minecraft;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class WorldAge {

    public static void beginNewEra(String path) {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(path));
            writer.write("0");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int loadData(String path) throws IOException {
        String age = Files.readString(Path.of(path), StandardCharsets.UTF_8);
        return Integer.parseInt(age);
    }
}

package com.darkyen.minecraft;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class Soul implements Comparable<Soul> {

    public final UUID ownerUuid;
    public final UUID worldUuid;
    public final double x, y, z;
    public final long timestamp;
    public final int xp;

    public Soul(@NotNull UUID ownerUuid, @NotNull UUID worldUuid, double x, double y, double z, long timestamp, int xp) {
        this.ownerUuid = ownerUuid;
        this.worldUuid = worldUuid;
        this.x = x;
        this.y = y;
        this.z = z;
        this.timestamp = timestamp;
        this.xp = xp;
    }

    @Override
    public String toString() {
        return "Soul{" +
                "ownerUuid=" + ownerUuid +
                ", worldUuid=" + worldUuid +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", timestamp=" + timestamp +
                ", xp=" + xp +
                '}';
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(@NotNull Soul o) {
        if (this.xp < o.xp) {
            return -1;
        } else if (this.xp > o.xp) {
            return 1;
        }
        return 0;
    }
}
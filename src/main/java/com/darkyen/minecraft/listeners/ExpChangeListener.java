package com.darkyen.minecraft.listeners;

import com.darkyen.minecraft.DeadSouls;
import com.darkyen.minecraft.Util;
import com.darkyen.minecraft.db.Leaderboard;
import com.darkyen.minecraft.db.LeaderboardEntry;
import io.papermc.lib.PaperLib;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;
import java.util.UUID;

import static com.darkyen.minecraft.DeadSouls.*;

public class ExpChangeListener implements Listener {

    private final HashSet<UUID> teleportingPlayers = new HashSet<>();

    public ExpChangeListener() {}

    private void updateMaxHealth(Player player, int level,  Leaderboard.Status status) {
        AttributeInstance maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH);
        if (maxHealth != null) {
            if (status == Leaderboard.Status.CHAMPION) {
                maxHealth.setBaseValue(24);
            } else if (level >= MIN_LEVEL_TO_BE_CHAMPION) {
                maxHealth.setBaseValue(22);
            } else if (level >= MIN_LEVEL_TO_HAVE_NORMAL_HP) {
                maxHealth.setBaseValue(20);
            } else {
                maxHealth.setBaseValue(18);
            }
        }
    }

    private void processPlayer(Player player, int level) {
        Leaderboard.Status status = inst.leaderboard.getStatus(player.getName());
        String colorPrefix = Leaderboard.getColorPrefix(status);
        player.setPlayerListName(colorPrefix+player.getName()+ChatColor.RESET+" "+soulsChar+level);
        updateMaxHealth(player, player.getLevel(), status);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerExpChangeEvent(PlayerExpChangeEvent event) {
        Player p = event.getPlayer();
        int xp = Util.getTotalExperience(p)+event.getAmount();
        inst.leaderboard.update(new LeaderboardEntry(p.getName(), System.currentTimeMillis(), xp, p.getLevel()));
        processPlayer(p, p.getLevel());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerLevelChangeEvent(PlayerLevelChangeEvent event) {
        Player p = event.getPlayer();
        int xp = Util.getTotalExperience(p);
        inst.leaderboard.update(new LeaderboardEntry(p.getName(), System.currentTimeMillis(), xp, event.getNewLevel()));
        processPlayer(p, event.getNewLevel());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        int xp = Util.getTotalExperience(p);
        inst.leaderboard.update(new LeaderboardEntry(p.getName(), System.currentTimeMillis(), xp, p.getLevel()));
        processPlayer(p, p.getLevel());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDeathEvent(PlayerDeathEvent event) {
        Player p = event.getEntity();
        inst.leaderboard.getEntries().remove(p.getName());
        processPlayer(p, 0);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageByBlockEvent(EntityDamageByBlockEvent event) {
        if (event.getDamage() > 1000) {
            return;
        }
        if (event.getEntity().getType().equals(EntityType.PLAYER)) {
            if (event.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {
                if (event.getEntity().getWorld().getUID().equals(inst.world_world.getUID())) {
                    Player p = (Player) event.getEntity();
                    Leaderboard.Status status = inst.leaderboard.getStatus(p.getName());
                    if (status == Leaderboard.Status.CHAMPION) {
                        UUID uuid = p.getUniqueId();
                        if (!teleportingPlayers.contains(uuid)) {
                            teleportingPlayers.add(uuid);
                            p.sendMessage(ChatColor.GRAY.toString()+ChatColor.ITALIC+"Бездна принимает Чемпиона");
                            Bukkit.getScheduler().runTaskLater(DeadSouls.inst, () -> PaperLib.teleportAsync(p, new Location(inst.world_bonfire, 8,22,25)).thenRun(new BukkitRunnable() {
                                @Override
                                public void run() {
                                    p.playSound(p.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, SoundCategory.AMBIENT, 1, 1);
                                    teleportingPlayers.remove(uuid);
                                }
                            }), 20*5);
                        }
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerQuitEvent(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        teleportingPlayers.remove(p.getUniqueId());

        if (Util.getTotalExperience(p) == 0) {
            inst.leaderboard.getEntries().remove(p.getName());
        }
    }
}
